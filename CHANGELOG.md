# Changelog

## 0.1 (2018-06-19)

- First release
- Only supports select
- Only supports file descriptor based events
