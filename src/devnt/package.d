// devnt/package.d - Event loop

module devnt;

public import devnt.base;
public import devnt.select;

/// The recommended event loop for the target.
alias EventLoop = SelectEventLoop;

/// Default event loop.
static EventLoop DefaultEventLoop;

static this() {
    DefaultEventLoop = new EventLoop();
}
