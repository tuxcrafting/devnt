// devnt/select.d - Select based event loop

module devnt.select;

import core.stdc.errno;
import core.sys.posix.sys.select;

import devnt.base;

/**
 * Select based event loop.
 * See_Also: IEventLoop
 */
class SelectEventLoop : IEventLoop {
    /// Is the event loop running?
    bool running;

    @trusted void set(int fd, Event event, EventCallback callback) {
        EventCallback[int]* map;

        final switch (event) {
        case Event.Read:
            map = &m_fd_read;
            break;
        case Event.Write:
            map = &m_fd_write;
            break;
        case Event.Error:
            map = &m_fd_error;
            break;
        }

        if (callback is null) {
            (*map).remove(fd);
        } else {
            (*map)[fd] = callback;
        }
    }

    @trusted int step(int timeout) {
        timeval t_timeout;
        t_timeout.tv_sec = timeout / 1000;
        t_timeout.tv_usec = timeout % 1000 * 1000;

        fd_set fd_r, fd_w, fd_e;
        int fd_max;

        FD_ZERO(&fd_r);
        FD_ZERO(&fd_w);
        FD_ZERO(&fd_e);

        foreach (int fd; m_fd_read.keys) {
            if (fd > fd_max) { fd_max = fd; }
            FD_SET(fd, &fd_r);
        }
        foreach (int fd; m_fd_write.keys) {
            if (fd > fd_max) { fd_max = fd; }
            FD_SET(fd, &fd_w);
        }
        foreach (int fd; m_fd_error.keys) {
            if (fd > fd_max) { fd_max = fd; }
            FD_SET(fd, &fd_e);
        }

        fd_max++;

        int n = select(fd_max, &fd_r, &fd_w, &fd_e, &t_timeout);

        if (n == -1 && errno == EINTR) {
            return 1;
        }

        if (n == 0) {
            return -1;
        }

        for (int fd = 0; fd < fd_max; ++fd) {
            if (FD_ISSET(fd, &fd_r)) {
                m_fd_read[fd](this, fd, Event.Read);
            }
            if (FD_ISSET(fd, &fd_w)) {
                m_fd_write[fd](this, fd, Event.Write);
            }
            if (FD_ISSET(fd, &fd_e)) {
                m_fd_error[fd](this, fd, Event.Error);
            }
        }

        return 0;
    }

    @trusted void run(int timeout) {
        running = true;
        while (running) {
            if (step(timeout) > 0) {
                running = false;
            }
        }
    }

    @trusted @nogc nothrow void stop() {
        running = false;
    }

private:
    EventCallback[int] m_fd_read, m_fd_write, m_fd_error;
}
