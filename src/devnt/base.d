// devnt/base.d - Event loop base

module devnt.base;

/// Which event to register.
enum Event {
    /// Data available.
    Read,

    /// Write ready.
    Write,

    /// Error.
    Error,
}

/**
 * Event callback.
 * Params:
 *      loop  = Event loop from which the event was raised
 *      fd    = File descriptor for which the event was raised
 *      event = Event for which the event was raised
 */
alias EventCallback = void delegate(IEventLoop loop, int fd, Event event);

/// Event loop interface.
interface IEventLoop {
    /**
     * Modify the events to listen to on a file descriptor.
     * In case the callback is null, then the event is unregistered.
     * Params:
     *      fd       = File descriptor
     *      events   = Event
     *      callback = Callback
     */
    @trusted void set(int fd, Event event, EventCallback callback);

    /**
     * Step the event loop until it times out or gets interrupted.
     * Params:
     *      timeout = Timeout in milliseconds
     * Returns: 0 in case an event was caught, -1 in case it timed out, or 1 in
     *          case of EINTR.
     */
    @trusted int step(int timeout);

    /**
     * Run the event loop until it gets interrupted.
     * Params:
     *      timeout = The timeout to use in each step.
     */
    @trusted void run(int timeout);

    /// Stop the event loop.
    @trusted @nogc nothrow void stop();
}
